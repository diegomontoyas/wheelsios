//
//  RightViewController.swift
//  Wheels
//
//  Created by Diego on 11/30/14.
//  Copyright (c) 2014 Diego. All rights reserved.
//

import UIKit

class RightViewController: UIViewController, FBLoginViewDelegate, UITextFieldDelegate
{
    @IBOutlet weak var fbLoginView: FBLoginView!
    @IBOutlet weak var grabber: UIView!
    @IBOutlet weak var recheckIntervalTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        recheckIntervalTextField.delegate = self
        
        fbLoginView.delegate = self
        
        //Bug hace que frame.size.height no retorne el valor correcto
        grabber.layer.cornerRadius = 40/2
        grabber.clipsToBounds = true
        
        fbLoginView.layer.cornerRadius = 10
        fbLoginView.clipsToBounds = true
        
        recheckIntervalTextField.text = "\(system.reCheckInterval)"
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        recheckIntervalTextField.endEditing(true)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        system.reCheckInterval = (recheckIntervalTextField.text! as NSString).doubleValue
    }
    
    func loginViewShowingLoggedOutUser(loginView: FBLoginView!)
    {
        navigationController?.popToRootViewControllerAnimated(true)
    }    
}
