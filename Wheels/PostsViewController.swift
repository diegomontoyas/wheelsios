//
//  ViewController.swift
//  Wheels
//
//  Created by Diego on 10/23/14.
//  Copyright (c) 2014 Diego. All rights reserved.
//

import UIKit
import AVFoundation

extension UIView
{
    func setCornerRadiusWithoutClipToBounds(cornerRadius:CGFloat)
    {
        let roundRectMaskPath = UIBezierPath(roundedRect: bounds, cornerRadius: CGFloat(5))
        
        let roundRectMaskLayer = CAShapeLayer()
        roundRectMaskLayer.frame = bounds
        roundRectMaskLayer.path = roundRectMaskPath.CGPath
        layer.mask = roundRectMaskLayer
    }
}

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, PostsDelegate
{
    @IBOutlet var timeTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var grabber: UIView!
    @IBOutlet weak var settingsButton: UIView!
    
    weak var mainPageViewController:MainPageViewController?
        
    let imageLoadingOperationQueue = NSOperationQueue()
    let imageProcessingOperationQueue = NSOperationQueue()

    private(set) var imageLoadingOngoingOperations = [NSIndexPath:NSBlockOperation]()
    
    let downloadedPhotoDimensions = "90"
    let photoImageViewSize:CGFloat = 52
    let nilImagePhotoImageViewSize:CGFloat = 14
    
    var posts = [Post]()
    
    var loadingData = false
    
    var player: AVPlayer?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        system.postsDelegate = self
        
        imageLoadingOperationQueue.qualityOfService = NSQualityOfService.UserInteractive
        imageProcessingOperationQueue.qualityOfService = NSQualityOfService.UserInteractive
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 50.0, 0.0)
        tableView.scrollsToTop = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 45
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(PostsViewController.grabberTapped(_:)))
        
        grabber.layer.cornerRadius = 20
        view.bringSubviewToFront(grabber)
        
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(PostsViewController.didLongPressCell(_:)))
        longPressGestureRecognizer.minimumPressDuration = 0.4
        longPressGestureRecognizer.delegate = self
        tableView.addGestureRecognizer(longPressGestureRecognizer)
        
        //grabber.addGestureRecognizer(tapGestureRecognizer)
        
        // Keeps the app running in the background
        let _ = try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: .MixWithOthers)
        let item = AVPlayerItem(URL: NSBundle.mainBundle().URLForResource("silence-10sec", withExtension: "mp3")!)
        player = AVPlayer(playerItem: item)
        player?.actionAtItemEnd = .None
        player?.play()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow
        {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        system.start()
    }
    
    func loadPostsInfoAndPrepareTableView()
    {
        posts = system.lastCheckPosts
    }
    
    func grabberTapped(sender:AnyObject)
    {
        mainPageViewController?.controllerGrabberPressed(self)
    }
    
    func systemDidReceiveNewPosts()
    {
        guard UIApplication.sharedApplication().applicationState == .Active else {
            return
        }
        
        loadPostsInfoAndPrepareTableView()
        
        dispatch_async(dispatch_get_main_queue()){
            
            self.tableView.reloadData()
        }
    }
    
    func systemDidDeleteUnnecessaryResources()
    {
        loadPostsInfoAndPrepareTableView()

        dispatch_async(dispatch_get_main_queue()){
            
            self.tableView.reloadData()
        }
    }
    
    //TableView DataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return posts.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return posts[section].comments.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let post = posts[indexPath.section]
        var cell:UITableViewCell
        
        if indexPath.row == 0
        {
            let postCell = tableView.dequeueReusableCellWithIdentifier("PostCell") as! PostCell
            
            postCell.photoImageView.image = nil
            
            if postCell.photoImageView.image == nil
            {
                postCell.photoWidthConstraint.constant = nilImagePhotoImageViewSize
                postCell.photoHeightConstraint.constant = nilImagePhotoImageViewSize
                
                postCell.layoutIfNeeded()
                postCell.updateConstraintsIfNeeded()
            }
            
            findPhotoForCell(postCell, post:post, atIndexPath: indexPath)
            
            postCell.contentView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
            
            postCell.textField.text = post.post
            postCell.label.text = post.senderName
            postCell.full = post.full
            
            postCell.photoImageView.clipsToBounds = true
            
            postCell.backBorderView.layer.cornerRadius = 8
            postCell.contentBackground.layer.cornerRadius = 8
            
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "MMMM dd hh:mm a"
            
            let timeInterval = 60*60*24*7 as NSTimeInterval
            
            let dateString = post.time?.timeAgoWithLimit(timeInterval, dateFormatter: dateformatter)
            
            postCell.timeLabel.text = dateString
            
            cell = postCell

        }
        else
        {            
            let comment = post.comments[indexPath.row-1]
            
            let commentCell = tableView.dequeueReusableCellWithIdentifier("CommentCell") as! CommentCell
            commentCell.textField.text = comment.comment
            commentCell.userInteractionEnabled = false
            
            commentCell.background.clipsToBounds = true
            commentCell.background.layer.cornerRadius = 8
            
            cell = commentCell
        }
        
        return cell
    }
    
    //TableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let post = posts[indexPath.row]
        system.goToPostPageOfPostWithID(post.ID)
    }
    
    func didLongPressCell(gestureRecognizer:UILongPressGestureRecognizer)
    {
        let point = gestureRecognizer.locationInView(tableView)
        let indexPath = tableView.indexPathForRowAtPoint(point)
        
        if indexPath != nil
        {
            let post = posts[indexPath!.row]
            
            system.goToProfilePageOfPersonWithID(post.senderID)
        }
    }
    
    func findPhotoForCell(cell:PostCell, post:Post, atIndexPath indexPath:NSIndexPath)
    {
        if let cachedPhoto = post.senderPhoto
        {
            cell.photoImageView?.image = cachedPhoto
            
            cell.photoWidthConstraint.constant = self.photoImageViewSize
            cell.photoHeightConstraint.constant = self.photoImageViewSize
            cell.photoImageView.layer.cornerRadius = self.photoImageViewSize/2
        }
        else
        {
            let blockOperation = NSBlockOperation()
            blockOperation.qualityOfService = NSQualityOfService.UserInteractive
            blockOperation.queuePriority = NSOperationQueuePriority.VeryHigh
            weak var weakBlockOperation = blockOperation
            
            blockOperation.addExecutionBlock() {
                
                dispatch_sync(dispatch_get_main_queue()) {
                    
                    if weakBlockOperation != nil && !weakBlockOperation!.cancelled
                    {
                        let params = ["redirect":"false","height":self.downloadedPhotoDimensions, "width":self.downloadedPhotoDimensions, "type":"normal"]
                        
                        FBRequestConnection.startWithGraphPath("/\(post.senderID)/picture", parameters:params, HTTPMethod: "GET", completionHandler: { (connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
                            
                            
                            if error == nil
                            {
                                let data = result["data"] as! FBGraphObject
                                let photoURLString = data["url"] as! String
                                let photoURL = NSURL(string: photoURLString)
                                
                                objc_sync_enter(self)
                                
                                if weakBlockOperation != nil && !weakBlockOperation!.cancelled
                                {
                                    objc_sync_exit(self)
                                    
                                    let request = NSMutableURLRequest(URL: photoURL!)
                                    request.HTTPMethod = "GET"
                                    request.timeoutInterval = 2;
                                    
                                    NSURLConnection.sendAsynchronousRequest(request, queue: self.imageProcessingOperationQueue, completionHandler: { (response:NSURLResponse?, photoData:NSData?, error:NSError?) -> Void in
                                        
                                        if photoData != nil
                                        {
                                            let photo = photoData != nil ? UIImage(data: photoData!) : nil
                                            
                                            if photo != nil
                                            {
                                                dispatch_sync(dispatch_get_main_queue()){
                                                    
                                                    post.senderPhoto = photo
                                                    
                                                    if weakBlockOperation != nil && !weakBlockOperation!.cancelled
                                                    {
                                                        cell.photoImageView.image = photo
                                                        cell.photoImageView.layer.cornerRadius = self.photoImageViewSize/2
                                                        
                                                        cell.layoutIfNeeded()
                                                        
                                                        UIView.animateWithDuration(0.2, animations: {
                                                            
                                                            cell.photoWidthConstraint.constant = self.photoImageViewSize
                                                            cell.photoHeightConstraint.constant = self.photoImageViewSize
                                                            
                                                            cell.layoutIfNeeded()
                                                            
                                                            }, completion: {(_) -> Void in
                                                                
                                                                cell.photoImageView.layer.cornerRadius = cell.photoImageView.frame.size.height/2
                                                        })
                                                    }
                                                    
                                                    self.imageLoadingOngoingOperations.removeValueForKey(indexPath)
                                                }
                                            }
                                        }
                                    })
                                }
                                else
                                {
                                    self.imageLoadingOngoingOperations.removeValueForKey(indexPath)
                                }
                            }
                        })
                    }
                    else
                    {
                        self.imageLoadingOngoingOperations.removeValueForKey(indexPath)
                    }
                }
            }
            
            self.imageLoadingOngoingOperations[indexPath] = blockOperation
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
                self.imageLoadingOperationQueue.addOperationAtFrontOfQueue(blockOperation)
            }
        }
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if let operation = imageLoadingOngoingOperations[indexPath]
        {
            operation.cancel()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        system.deleteUnnecessaryResources()
    }
}
