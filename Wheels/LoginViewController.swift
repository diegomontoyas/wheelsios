//
//  LoginViewController.swift
//  Wheels
//
//  Created by Diego on 10/30/14.
//  Copyright (c) 2014 Diego. All rights reserved.
//

import Foundation

class LoginViewController: UIViewController, FBLoginViewDelegate
{
    @IBOutlet var fbLoginView : FBLoginView!
    @IBOutlet var wheel: UIImageView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        fbLoginView.delegate = self
        
        if kUseFacebookDeveloperConnection
        {
            fbLoginView.readPermissions = ["user_groups"]
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.appDidBecomeActive(_:)), name: "appDidBecomeActive", object: nil)
    }
    
    func appDidBecomeActive(notification: NSNotification)
    {
        if isViewLoaded() && view.window != nil
        {
            animateWheel()
        }
    }
    
    override func viewDidAppear(animated: Bool)
    {
        animateWheel()
    }
    
    func animateWheel ()
    {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0.0
        animation.toValue = 2*M_PI
        animation.duration = 6
        animation.repeatCount = 1000
        wheel.layer.addAnimation(animation, forKey:"rotation")
    }
    
    // Facebook Delegate Methods
    
    func loginViewShowingLoggedInUser(loginView : FBLoginView!)
    {
        print("User Logged In")
        
        transitionToPostsViewController()
    }
    
    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser)
    {
        print("User: \(user)")
        print("User ID: \(user.objectID)")
        print("User Name: \(user.name)")
    }
    
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!)
    {
        print("User Logged Out")
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError)
    {
        print("Error: \(handleError.localizedDescription)")
    }

    func transitionToPostsViewController()
    {
        let viewController = storyboard?.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
}
