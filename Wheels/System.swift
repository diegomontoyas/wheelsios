//
//  System.swift
//  Wheels
//
//  Created by Diego on 10/30/14.
//  Copyright (c) 2014 Diego. All rights reserved.
//

import Foundation
import AudioToolbox

protocol PostsDelegate: class
{
    func systemDidReceiveNewPosts()
    
    func systemDidDeleteUnnecessaryResources()
}

private let _systemSharedInstance = System()
let system =  System.S()

let serverIP = "157.253.238.87"
let serverPort = "8080"
let serverAppPath = ""
let serverPath = "http://\(serverIP):\(serverPort + serverAppPath)"

let kWheelsGroupID = "429208293784763"

let kUseFacebookDeveloperConnection = true

class System: NSObject
{
    var reCheckInterval = 2.0
    {
        didSet
        {
            if started
            {
                timer.invalidate()
                scheduleTimer()
            }
        }
    }
    
    weak var postsDelegate:PostsDelegate?
    
    private(set) var lastCheckPosts = [Post]()
    private var mutablePosts = [Post]()
    private var postsDictionary = [String:Post]()

    private(set) var filters = [String]()
    
    private(set) var currentlyChecking = false
    
    private var checkLock = NSObject()
    
    private(set) var started = false
    private var timer: NSTimer!
    
    private var checkOperationCount = 0
    
    private var checkAfterFilterChangeTimer: NSTimer?
    
    override init()
    {
        super.init()
    }
    
    class func S() -> System
    {
        return _systemSharedInstance
    }
    
    func start()
    {
        if !started
        {
            started = true
            self.reCheckDeletingRecentPosts(true)
            scheduleTimer()
        }
    }
    
    func scheduleTimer()
    {
        timer = NSTimer.scheduledTimerWithTimeInterval(self.reCheckInterval, target: self, selector: #selector(System.postsTimerTicked(_:)), userInfo: nil, repeats: true)
    }
    
    func postsTimerTicked(timer:NSTimer)
    {
        self.reCheckDeletingRecentPosts(false)
    }
    
    func reCheckDeletingRecentPosts(deletingRecentPosts:Bool, force:Bool = false)
    {
        if checkOperationCount == 2 && !force { return }
        
        checkOperationCount += 1
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let urlString = "\(serverPath)/getFeed"
        let url = NSURL(string: urlString)
        
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        request.timeoutInterval = 2;
        
        let certPath = NSBundle.mainBundle().pathForResource("wheelsserver", ofType:"keystore")
        let certificateData = NSData(contentsOfFile: certPath!)
        
        let connection = WrappedNSURLConnection(request: request)
        connection.certificateData = certificateData
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) {(response:NSURLResponse?, data:NSData?, error:NSError?) -> Void in
            
            if data != nil
            {
                print("Received from backend: \(NSDate())")
                
                self.checkOperationCount -= 1
                
                if self.checkOperationCount == 0
                {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                }
                
                let JSONResponse = (try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary!
                let postsJSON = JSONResponse["data"] as! [AnyObject]
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
                
                    self.receivedFacebookPostsInfoWithJSONData(postsJSON, error: error, deleteRecentPosts:deletingRecentPosts)
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue()){
                    
                    let limit = deletingRecentPosts ? 100 : 10
                    
                    FBRequestConnection.startWithGraphPath("\(kWheelsGroupID)/feed?limit=\(limit)", completionHandler: { (connection: FBRequestConnection?, result: AnyObject?, error: NSError?) -> Void in
                        
                        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
                        
                            guard var JSONPosts = result?["data"] as? [AnyObject] where error == nil else
                            {
                                self.checkOperationCount -= 1
                                return
                            }
                            
                            print("Didn't receive from backend, received from facebook: \(NSDate())")
                            
                            var currentJSONPaging = result?["paging"] as? NSDictionary
                            
                            while let nextPageURLString = currentJSONPaging?["next"] as? String
                            {
                                let nextPageURL =  NSURL(string: nextPageURLString)!
                                let request = NSMutableURLRequest(URL: nextPageURL)
                                request.HTTPMethod = "GET"
                                request.timeoutInterval = 2
                                
                                guard let pagingData = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil),
                                      let currentJSONPagingResponse = (try? NSJSONSerialization.JSONObjectWithData(pagingData, options: NSJSONReadingOptions.MutableContainers)) as? [String : AnyObject],
                                      let JSONArray = currentJSONPagingResponse["data"] as? [AnyObject]
                                else
                                {
                                    break
                                }
                                
                                
                                JSONPosts += JSONArray
                                
                                if JSONPosts.count >= limit
                                {
                                    break
                                }
                                
                                currentJSONPaging = currentJSONPagingResponse["paging"] as? [String : AnyObject]
                            }
                            
                            self.checkOperationCount -= 1
                            
                            if self.checkOperationCount == 0
                            {
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            }
                            
                            self.receivedFacebookPostsInfoWithJSONData(JSONPosts, error: error, deleteRecentPosts:deletingRecentPosts)
                        }
                    })
                    return
                }
            }
        }
    }
    
    private func receivedFacebookPostsInfoWithJSONData(JSONData: [AnyObject]!, error: NSError!, deleteRecentPosts:Bool)
    {
        if deleteRecentPosts
        {
            self.mutablePosts = []
            self.postsDictionary = [:]
        }
        
        guard error == nil else { return }
        
        let postsJSON = JSONData
        var newPosts = [Post]()
        
        for rawPost in postsJSON
        {
            var comments = [Comment]()
            
            let possibleMessageJSON = rawPost["message"] as! String?
            
            guard let message = possibleMessageJSON else { return }
            
            let possibleCommentsJSON = rawPost["comments"] as! NSDictionary?
            let postID = rawPost["id"] as! String
            
            var containsFilters = false
            let containsTime = true /*= messageJSON.contains(timeTextField.text) || timeTextField.text.isEmpty*/
            
            for filter in filters where message.contains(filter)
            {
                containsFilters = true
                break
            }
            
            var full = false
            
            if let commentsJSON = possibleCommentsJSON
            {
                // if Facebook
                let commentDataJSON = commentsJSON["data"] as! [AnyObject]
                
                for commentJSON in commentDataJSON
                {
                    let messageCommentJSON = commentJSON["message"] as! String
                    
                    let comment = Comment(comment: messageCommentJSON)
                    comments.append(comment)
                    
                    let postSenderJSON = rawPost["from"] as! NSDictionary
                    let postSenderID = postSenderJSON["id"] as! String
                    
                    let commentSenderJSON = commentJSON["from"] as! NSDictionary
                    let commentSenderID = commentSenderJSON["id"] as! String
                    
                    if commentSenderID == postSenderID
                    {
                        if messageCommentJSON.contains("lleno") || messageCommentJSON.contains("llena")
                            || ( messageCommentJSON.contains("no") && (messageCommentJSON.contains("quedan") || messageCommentJSON.contains("hay") || messageCommentJSON.contains("tengo")) && messageCommentJSON.contains("cupos") )
                        {
                            full = true
                        }
                    }
                }
            }
            
            if containsTime && (containsFilters || filters.isEmpty)
            {
                if let existingPost = postsDictionary[postID]
                {
                    if message != existingPost.post { existingPost.post = "E: " + message }
                    
                    existingPost.comments = comments
                    existingPost.full = full
                }
                else
                {
                    let from = rawPost["from"] as! NSDictionary
                    let senderName = from["name"] as! String
                    let senderID = from["id"] as! String
                    let time = rawPost["created_time"] as! String
                    
                    let post = Post(ID:postID, senderName: senderName, senderID: senderID, post: message, time:convertFacebookTimeStringToNSDate(time), full:full)
                    
                    /*var post = Post(ID:postID, senderName: senderName, senderID: senderID, post: messageJSON, time:convertServerTimeStringToNSDate(time), full:full)*/
                    
                    post.comments = comments
                    mutablePosts.insert(post, atIndex: mutablePosts.insertionSortedIndex(post, isOrderedBefore: >))
                    postsDictionary[postID] = post
                    newPosts.append(post)
                }
            }
        }
        
        lastCheckPosts = mutablePosts
        
        if !deleteRecentPosts && !newPosts.isEmpty
        {
            self.vibrateAndNotify(newPosts: newPosts)
        }
        
        self.postsDelegate?.systemDidReceiveNewPosts()
    }
    
    func vibrateAndNotify(newPosts newPosts: [Post])
    {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
     
        dispatch_async(dispatch_get_main_queue()) {
            
            guard UIApplication.sharedApplication().applicationState != .Active else { return }
            
            for post in newPosts
            {
                let notification = UILocalNotification()
                notification.fireDate = NSDate()
                notification.alertBody = post.post
                notification.soundName = UILocalNotificationDefaultSoundName
                notification.userInfo = ["id": post.ID]
                UIApplication.sharedApplication().scheduleLocalNotification(notification)
            }
        }
    }
    
    func notifyPost(post:Post)
    {
        print("new post")
    }
    
    func addFilter(filter:String)
    {
        filters.append(filter)
        schedulePostCheckDeletingRecentPostsAfterInterval()
    }
    
    func removeFilterAtIndex(index:Int)
    {
        filters.removeAtIndex(index)
        schedulePostCheckDeletingRecentPostsAfterInterval()
    }
    
    private func schedulePostCheckDeletingRecentPostsAfterInterval ()
    {
        if checkAfterFilterChangeTimer != nil && checkAfterFilterChangeTimer!.valid
        {
            checkAfterFilterChangeTimer!.invalidate()
        }
        
        let block = NSBlockOperation {
            system.reCheckDeletingRecentPosts(true, force: true)
        }
        
        checkAfterFilterChangeTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: block, selector: #selector(NSOperation.main), userInfo: nil, repeats: false)
    }
    
    func goToPostPageOfPostWithID(ID: String)
    {
        UIApplication.sharedApplication().openURL( NSURL(string:"http://facebook.com/"+ID)!)
    }
    
    func goToProfilePageOfPersonWithID(ID: String)
    {
        UIApplication.sharedApplication().openURL( NSURL(string:"http://facebook.com/"+ID)!)
    }
    
    func convertFacebookTimeStringToNSDate(time:String) -> NSDate
    {
        let df = NSDateFormatter()
        df.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ssZ"
        
        let date = df.dateFromString(time)
        return date!
    }
    
    func convertServerTimeStringToNSDate(time:String) -> NSDate
    {
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy HH:mm:ss"
        
        let date = df.dateFromString(time)
        return date!
    }
    
    func deleteUnnecessaryResources()
    {
        for post in mutablePosts
        {
            post.senderPhoto = nil
            
            dispatch_async(dispatch_get_main_queue())
                {
                    self.postsDelegate?.systemDidReceiveNewPosts()
                    ()
            }
        }
    }
}

extension String
{
    func contains(string: String) -> Bool
    {
        return self.lowercaseString.rangeOfString(string.lowercaseString) != nil
    }
}

extension Array {
    func insertionSortedIndex(elem: Element, isOrderedBefore: (Element, Element) -> Bool) -> Int {
        var lo = 0
        var hi = self.count - 1
        while lo <= hi {
            let mid = (lo + hi)/2
            if isOrderedBefore(self[mid], elem) {
                lo = mid + 1
            } else if isOrderedBefore(elem, self[mid]) {
                hi = mid - 1
            } else {
                return mid // found at position mid
            }
        }
        return lo // not found, would be inserted at position lo
    }
}


